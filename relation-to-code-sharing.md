# Relation to code sharing

Programming literacy is one challenge. Sharing source codes for people having a 
programing literacy is a challenge for this restricted group of people.

Code sharing is part of the practices of many groups, being internally or on the
web. Scientific funding agencies go towards open licences by default for all
scientific productions, be it code, data or publications (see [](relation-to-data-sharing.md)
and [](relation-to-publication-sharing.md)).

Yet, the adoption of code sharing practices or policies faces challenges.

## Study

## Tools to keep traces

## Effects of the tools on cooperation

## Conclusion