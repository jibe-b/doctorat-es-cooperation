## How to contribute

All types of contributions to this project are welcome.

Please open [issues](https://gitlab.com/jibe-b/doctorat-es-cooperation/issues) for any reasonable reason and consider making pull requests if you wrote some code or content that you think would benefit to the project.

You may also contact me at the address user701@orange.fr
