# Doctorat _ès_ coopération

## Research project

This research project is an open collaboration project aiming to collect digital traces during the development of research projects with various sets of organizational rules to evaluate the impact of such rules on the development patterns of the project.

Most of the development is currently done in the [issues tracker](https://gitlab.com/jibe-b/doctorat-es-cooperation/issues).

## Contributing to this open research project

This project is organized to be as open as possible to contributions, please consider contributing :) (you may also read the [contribution guide](CONTRIBUTING.md))


## A thesis

This part is a personal contribution, a challenge about the way I can contribute to science.

### Chapter 1

Sharing data, code, information is not obvious. What are the behaviours, opinions
that can be observed?

[Case 1: sharing data produced during scientific research](relation-to-data-sharing.md)

### Conclusion

[Evolutive conclusion of the study of cooperation through digital tools](conclusion.md)