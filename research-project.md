# Dynamics of cooperation and organizational rules, from traces to models

State of the document: this document is in progress, expect some missing parts. Reviews are highly welcome, please open a [new issue](https://gitlab.com/jibe-b/doctorat-es-cooperation/issues).

Root of the project: [jibe-b/doctorat-es-cooperation](https://gitlab.com/jibe-b/doctorat-es-cooperation/)

## Project presentation

### Abstract

The dynamics of cooperation is constrained by organization rules that define the accessible area of the space of possibilities and its topology. These rules operate on interaction between individuals as well as on individuals outside of a specific project.
The digital traces of contributions to projects, including peripherical exchanges between actors are either already captured or available for capture, at the price of developing specific trackers.
The respect of privacy requires that the data acquisition and analysis be designed so that the data remains in the hands of the individuals and data be aggregated only for the analyses for which it is mathematically to do so.
We consider this challenge as an opportunity to involve citizens in the analysis of their own data.

[Draft](https://gitlab.com/jibe-b/doctorat-es-cooperation/issues/11)


### State of the art 

This section is in progress. Please do not review this version.

1. Approaches

  - longitudinal
  - snapshots
  - forward experiment

2. Domains

  - science of science
  - contribution to code repos and wikis
  - educa:tion

3. Known mechanisms

4. Projects under way

[Draft](https://gitlab.com/jibe-b/doctorat-es-cooperation/issues/3)

### Question and axis of research

#### Observation

Up to now, the study of project progression do not involve experimentation. The experience gained from experimentating on educational pathways may be used to experiment on project progression at the time of realization.

#### Research question(s)

How do organizational rules influence the dynamics of progression of a project?

#### Research hypothesis

Some tendencies of a project's progression dynamics are assumed to be significantly correlated to the organization rules of the project.

The tools currently available enable to capture the explicit organizational rules of a project and the digital traces of the individual activities along the progression of the project. These tools may be used to carry experiments on the influence of organizational rules on the progression of the project.

### Experiment design

#### Measure

Projects involving the use of digital tools result in the production of digital traces. Digital tools providers collect such data and some of this data is accessible to a user. Supplementary tracking data may be obtained by tracking the digital activity of users who choose to install a tracking software.

#### Experimentation

The organizational rules can be designed or perturbated at the level of individuals. Technical constraints can be enforced or removed to influence the realization of specific cooperation interactions. 

#### Analysis

The analysis of the datasets involve a data mining approach in order to identify the non-negligible effects, as well as a robust training of models specific to the identified mechanisms.

#### Interpretation

Interpretation rules will be implemented as code in order to achieve automated production of reports.

### Cohort recruitment

The project in its first stage is focused towards the development of the experiment setup. Example cohorts will be recruited among known groups in the network of the project contributors or from externals networks.

As a citizen science project, recruitment and carrying of experiments on cohorts will be suggested to external individuals and may be carried in the frame of the project by the core development team.

## Implementation

### Experimental setup

The data aggregators consists in API harvesters which get the traces captured by online services as well as WebExtensions tracking the activity of users using webapps.

Additional individual profiling is achieved through surveys filled by the individuals.

Experimentation is achieved by enforcing organizational rules at the level of the project as well as technical modifications of the user's workflow. This may include customizing UIs, providing apps easing specific tasks or providing specific data to the user.

[Draft](https://gitlab.com/jibe-b/doctorat-es-cooperation/issues/4)

### Data management plan

The data targeted by the experiment is in the category of privacy data. In order to respect and protect the privacy of users, all data will be collected and stored in a place controlled by the user. Data harvesters will store data in a user controlled device and trackers will store data on the client.

GDPR regulation will be strictly enforced.

Part of the data analysis only requires the data of a user to be carried on and computation power corresponding to the computational power of an average user's device. Such data analyses will be carried on the client. For analyses that require high computational power or transversal aggregation of data, an opt-in of the user will be required for the federation of the data and its analysis. Federated data will be managed centrally and erased when no longer needed.

Among the data gathered and analysed on a user's client, which data that user has to send the experiment leaders will be defined in the participation contract. If no contract has been signed, users are free to make use of the data according to their own will.

All anonymised and unidentifiable (according to the state of the art) data that the users consent to share will be released to under a free licence (CC-BY-ND or CC0).

[Draft 1](https://gitlab.com/jibe-b/doctorat-es-cooperation/issues/5)

[Draft 2](https://gitlab.com/jibe-b/doctorat-es-cooperation/issues/8)


### Technical choices

All of the following choices are needed to be made in order for an individual to work efficiently on the project. A federation of workers on the project may lead to the cohabitation of different choices, with the only requirement of data interoperability.

- Framework for the aggregation setup: [Cozy](cozy.io). [Reasons to choose Cozy](https://gitlab.com/jibe-b/doctorat-es-cooperation/issues/12).
- Tracking user's digital activity: WebExtensions. [Reasons to choose WebExtensions](https://gitlab.com/jibe-b/doctorat-es-cooperation/issues/13).

### Data analysis

(in progress)


### Interpretation

Interpretation models will be included with the standalone analysis methods.

A particular care will be made in making the _limitations_ of the experiment as well as the interpretation unequivocally understandable.


## Discussion

### Ethics

The choice to perturbate the progress of a project may lead to minor ethics concerns.

Aggregating and tracking additional activity data may lead to major ethics concerns, notably in terms of risk of leak of such data and bad use by third parties.

### Challenges

[List of identified challenges](https://gitlab.com/jibe-b/doctorat-es-cooperation/issues/10)


### Citizen science

The design of the experiment requires individuals wider than the core development team to carry the experimentation specific cohorts.

Individuals member of the cohorts may as well be involved, which then requires to study the effect of involvment in the project on the user's activity.


## Work plan

### Roadmap

1. Preliminary description of the project and discussion with peers

2. Development of data acquisition connectors

3. Data mining of aggregated data

4. Training of models for specific mechanisms

5. Parallel progression of actions 2, 3, 4

[Roadmap board](https://gitlab.com/jibe-b/doctorat-es-cooperation/boards/)


### Milestones

1. M1: [release of a functional data aggregation set of tools](https://gitlab.com/jibe-b/doctorat-es-cooperation/milestones/1)

2. M2: [release of a set of models specific to observed mechanisms](https://gitlab.com/jibe-b/doctorat-es-cooperation/milestones/2)

### Evaluation tests

Statistical tests and limitations tests will be developed and results of such tests will be provided as first class information.



## Organizational rules


### Open contribution and forkability

This project is intended for open cooperation. The [contribution guide](CONTRIBUTING.md) stipulates the organizational rules applying to the cooperation on this project.

All content is under the MIT licence, which enables one to fork the project and take the lead and get sovereign in terms of subjectivity of the (forked) project

[Draft](https://gitlab.com/jibe-b/doctorat-es-cooperation/issues/6)


### Members

This project starts as an individually lead project. As a result, the first contents reflect the subjectivity of the repo owner.

Membership is based on contribution to the project and on the acceptance of the [charter](charter.md).

[Draft](https://gitlab.com/jibe-b/doctorat-es-cooperation/issues/1)

A subjectivity disclaimer is available [here](https://gitlab.com/jibe-b/doctorat-es-cooperation/issues/9).

Members will be cited in the repo.


## Contact

Please reach me at user701 @ orange.fr
