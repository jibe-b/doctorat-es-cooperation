# Results sharing

Scientific literacy is one challenge. Results sharing for people having a scientific literacy
is a challenge for this restricted group of people.

Result sharing is part of the practices of many groups, being internally or on the
web. Scientific funding agencies go towards open licences by default for all
scientific productions, be it publications, data or code (see [](relation-to-data-sharing.md)
and [](relation-to-code-sharing.md)).

Yet, the adoption of results sharing practices or policies faces challenges.

## Study

## Tools to keep traces

## Effects of the tools on cooperation

## Conclusion