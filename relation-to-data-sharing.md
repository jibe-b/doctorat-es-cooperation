# Data sharing

Data literacy is one challenge. Data sharing for people having a data literacy
is a challenge for this restricted group of people.

Data sharing is part of the practices of many groups, being internally or on the
web. Scientific funding agencies go towards open licences by default for all
scientific productions, be it data, code or publications (see [](relation-to-code-sharing.md)
and [](relation-to-publication-sharing.md)).

Yet, the adoption of data sharing practices or policies faces challenges.

## Study

## Tools to keep traces

## Effects of the tools on cooperation

## Conclusion